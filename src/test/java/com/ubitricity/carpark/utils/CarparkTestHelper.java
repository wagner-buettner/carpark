package com.ubitricity.carpark.utils;

import com.ubitricity.carpark.dto.CarparkDto;
import com.ubitricity.carpark.persistence.model.Carpark;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class CarparkTestHelper {

    public static CarparkDto getCarparkDtoWithId(Long id) {
        CarparkDto carpark = CarparkDto.builder()
            .chargingPoint("CP".concat(id.toString()))
            .fastCharging(true)
            .currentEnergy(20)
            .startDatetime(LocalDateTime.now())
            .available(false)
            .build();

        return carpark;
    }

    public static Carpark getCarparkWithId(Long id) {
        Carpark carpark = Carpark.builder()
            .id(id)
            .chargingPoint("CP".concat(id.toString()))
            .fastCharging(true)
            .currentEnergy(20)
            .startDatetime(LocalDateTime.now())
            .available(false)
            .build();

        return carpark;
    }

    public static List<Carpark> getCarparkListWithId() {
        return Arrays.asList(getCarparkWithId(1L), getCarparkWithId(2L), getCarparkWithId(3L));
    }

    public static List<Carpark> getInitialChargingPointsList(Integer numberOfChargingPoints){
        List<Carpark> carparks = new ArrayList<>();
        IntStream.range(1,numberOfChargingPoints + 1).forEach(index ->
            carparks.add(Carpark.builder()
                .id(Long.valueOf(index))
                .chargingPoint("CP" + index)
                .available(true)
                .currentEnergy(0)
                .fastCharging(false)
                .build())
        );

        return carparks;
    }
}
