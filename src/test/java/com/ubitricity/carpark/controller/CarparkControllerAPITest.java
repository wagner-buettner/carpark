package com.ubitricity.carpark.controller;

import com.ubitricity.carpark.AbstractIntegrationTest;
import com.ubitricity.carpark.persistence.repository.CarparkRepository;
import com.ubitricity.carpark.utils.CarparkTestHelper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static com.ubitricity.carpark.util.Constants.CARPARK_URI_WITH_ID_CONNECT;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CarparkControllerAPITest extends AbstractIntegrationTest {

    @Autowired
    private CarparkController controller;

    @Autowired
    private CarparkRepository repository;

    // TODO: Cuidar para inserir os dados antes de cada teste, pois tá sendo deletado
    @Before
    public void setup() {
        repository.deleteAll();
        CarparkTestHelper.getInitialChargingPointsList(10)
            .forEach( carpark -> repository.save(carpark));

        initMockMvc(controller);
    }

    @Test
    public void updateChargingPointCP1_Plugging_Success() throws Exception {

        mockMvc.perform(put(CARPARK_URI_WITH_ID_CONNECT, "CP1")
               .contentType(MediaType.APPLICATION_JSON).content("{}"))

        //then
            .andExpect(status().is2xxSuccessful())
            .andExpect(jsonPath("$.*", hasSize(Matchers.greaterThanOrEqualTo(10))))
            .andExpect(jsonPath("$.[0].startDatetime").exists())
            .andExpect(jsonPath("$.[0].currentEnergy").value(20L));
    }

}
