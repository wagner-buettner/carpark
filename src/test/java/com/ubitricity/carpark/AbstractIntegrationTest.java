package com.ubitricity.carpark;

import com.ubitricity.carpark.exceptionhandler.CarparkExceptionHandler;
import org.json.JSONException;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.RegularExpressionValueMatcher;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testcontainers.containers.PostgreSQLContainer;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarparkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = AbstractIntegrationTest.Initializer.class)
public abstract class AbstractIntegrationTest {

    private static final String DATABASE_NAME = "carpark";
    private static final PostgreSQLContainer postgres = new PostgreSQLContainer().withDatabaseName(DATABASE_NAME);

    protected MockMvc mockMvc;

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    static {
        postgres.start();
    }

    protected void initMockMvc(Object controller) {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setControllerAdvice(CarparkExceptionHandler.class)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
    }

    protected void checkJson(String expected, String result) throws JSONException {

        JSONAssert.assertEquals(expected, result, new CustomComparator(JSONCompareMode.LENIENT,
                new Customization("id", new RegularExpressionValueMatcher<>("\\d"))));
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues values = TestPropertyValues.of(
                    "spring.datasource.driver-class-name=" + postgres.getDriverClassName(),
                    "spring.datasource.url=" + postgres.getJdbcUrl(),
                    "spring.datasource.username=" + postgres.getUsername(),
                    "spring.datasource.password=" + postgres.getPassword(), "spring.jpa.hibernate.ddl-auto=update",
                    "spring.jpa.hibernate.database=" + DATABASE_NAME,
                    "spring.jpa.hibernate.database-platform=org.hibernate.dialect.PostgreSQLDialect",
                    "spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults=false",
                    "spring.flyway.locations=classpath:db/migration");
            values.applyTo(configurableApplicationContext);
        }
    }
}
