package com.ubitricity.carpark.service;

import com.ubitricity.carpark.AbstractIntegrationTest;
import com.ubitricity.carpark.dto.CarparkDto;
import com.ubitricity.carpark.persistence.model.Carpark;
import com.ubitricity.carpark.persistence.repository.CarparkRepository;
import com.ubitricity.carpark.utils.CarparkTestHelper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class CarparkServiceTest extends AbstractIntegrationTest {

    private CarparkService service;

    private ModelMapper modelMapper;

    @Mock
    private CarparkRepository repository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        modelMapper = new ModelMapper();
        service = new CarparkService(repository, modelMapper);
    }

    @Test
    public void findAllChargingPoints_Valid() {
        List<Carpark> carparks = CarparkTestHelper.getCarparkListWithId();

        given(repository.findAllByOrderByIdAsc()).willReturn(carparks);

        List<CarparkDto> carparkService = service.findAllByOrderByIdAsc();

        verify(repository, Mockito.times(1)).findAllByOrderByIdAsc();
        assertEquals(carparks.size(), carparkService.size());
    }

    @Test
    public void findByChargingPoint_Valid() {
        long carparkId = 1L;
        Carpark carpark = CarparkTestHelper.getCarparkWithId(carparkId);

        given(repository.findByChargingPoint(carpark.getChargingPoint())).willReturn(carpark);

        CarparkDto carparkService = service.findByChargingPoint(carpark.getChargingPoint());

        assertThat(carparkService.getChargingPoint()).isEqualTo(carpark.getChargingPoint());
        assertThat(carparkService.getCurrentEnergy()).isEqualTo(carpark.getCurrentEnergy());
    }

    @Test
    public void validadeIfChargingIsNotAvailable_Valid() {
        long carparkId = 1L;
        Carpark carpark = CarparkTestHelper.getCarparkWithId(carparkId);

        given(repository.findByChargingPoint(carpark.getChargingPoint())).willReturn(carpark);

        CarparkDto carparkService = service.findByChargingPoint(carpark.getChargingPoint());

        assertThat(carparkService.getAvailable()).isEqualTo(carpark.isAvailable());
    }

    // TODO : implementar testes unitarios
    // Teste para quando o charging point não tiver disponível?
    // E o teste de conexão? E o teste de quando não conseguir conectar?
    // Teste para quando se passa o id errado?
    // Teste para desconectar com sucesso, teste para não conseguir desconectar
}
