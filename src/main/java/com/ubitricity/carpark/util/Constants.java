package com.ubitricity.carpark.util;

public class Constants {
    public static final String CARPARK_URI = "/v1/chargingpoints";
    public static final String CARPARK_URI_WITH_ID = "/v1/chargingpoints/{chargingPoint}";
    public static final String CARPARK_URI_WITH_ID_CONNECT = "/v1/chargingpoints/{chargingPoint}/connect";
    public static final String CARPARK_URI_WITH_ID_DISCONNECT = "/v1/chargingpoints/{chargingPoint}/disconnect";

    public static final int TOTAL_AVAILABLE_ENERGY = 100;
    public static final int FAST_CHARGE = 20;
    public static final int NORMAL_CHARGE = 10;

}
