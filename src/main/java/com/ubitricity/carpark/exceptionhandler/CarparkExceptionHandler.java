package com.ubitricity.carpark.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
public class CarparkExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        String userMessage = messageSource.getMessage("invalid.message", null, LocaleContextHolder.getLocale());
        String developerMessage = ex.getCause() != null ? ex.getCause().toString() : ex.toString();

        List<Error> erros = Arrays.asList(new Error(userMessage, developerMessage));
        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<Error> errorList = createErrorList(ex.getBindingResult());
        return handleExceptionInternal(ex, errorList, headers, HttpStatus.BAD_REQUEST, request);
    }

    private List<Error> createErrorList(BindingResult bindingResult) {
        List<Error> errorList = new ArrayList<>();

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            String userMessage = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            String developerMessage = fieldError.toString();
            errorList.add(new Error(userMessage, developerMessage));
        }
        return errorList;
    }

    @ExceptionHandler({ ChargingPointAlreadyConnected.class })
    public ResponseEntity<Object> handleChargingPointAlreadyConnectedException(ChargingPointAlreadyConnected ex) {
        String userMessage = messageSource.getMessage("charging_point.already_connected", null,
                LocaleContextHolder.getLocale());
        String developerMessage = ex.getMessage();

        List<Error> errorList = Arrays.asList(new Error(userMessage, developerMessage));
        return ResponseEntity.badRequest().body(errorList);
    }

    @ExceptionHandler({ ChargingPointNotExist.class })
    public ResponseEntity<Object> handleChargingPointNotExistException(ChargingPointNotExist ex) {
        String userMessage = messageSource.getMessage("charging_point.not_exist", null,
                LocaleContextHolder.getLocale());
        String developerMessage = ex.getMessage();

        List<Error> errorList = Arrays.asList(new Error(userMessage, developerMessage));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorList);
    }
}
