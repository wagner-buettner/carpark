package com.ubitricity.carpark.exceptionhandler;

public class ChargingPointNotExist extends RuntimeException {

    private static final long serialVersionUID = 6061414217226791940L;

    public ChargingPointNotExist(String chargingPoint) {
        super("Charging point " + chargingPoint + " does not exist.");
    }
}
