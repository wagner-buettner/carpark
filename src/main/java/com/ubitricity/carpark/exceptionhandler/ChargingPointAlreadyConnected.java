package com.ubitricity.carpark.exceptionhandler;

public class ChargingPointAlreadyConnected extends RuntimeException {
    private static final long serialVersionUID = -2605047129041452908L;

    public ChargingPointAlreadyConnected(String chargingPoint) {
        super("Charging point already connected, ignoring request for " + chargingPoint + ".");
    }
}
