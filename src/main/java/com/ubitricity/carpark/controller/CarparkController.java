package com.ubitricity.carpark.controller;

import com.ubitricity.carpark.dto.CarparkDto;
import com.ubitricity.carpark.persistence.model.Carpark;
import com.ubitricity.carpark.service.CarparkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ubitricity.carpark.util.Constants.*;

@RestController
@Api(value = "Carpark Charging Points", tags = {"Charging Points"})
public class CarparkController {

    private final CarparkService carparkService;

    public CarparkController(CarparkService carparkService) {
        this.carparkService = carparkService;
    }

    @GetMapping(CARPARK_URI)
    @ApiOperation(value = "Get all charging points", nickname = "getChargingPoints")
    public ResponseEntity<List<CarparkDto>> findAll() {
        return ResponseEntity.ok(carparkService.findAllByOrderByIdAsc());
    }

    @GetMapping(CARPARK_URI_WITH_ID)
    @ApiOperation(value = "Get charging point by charging point id", nickname = "getChargingPointByChargingPoint")
    public ResponseEntity<CarparkDto> findByChargingPoint(@PathVariable String chargingPoint) {
        return ResponseEntity.ok(carparkService.findByChargingPoint(chargingPoint));
    }

    @PutMapping(CARPARK_URI_WITH_ID_CONNECT)
    @ApiOperation(value = "Update charging point connecting a car", nickname = "connectChargingPoint")
    public ResponseEntity<List<CarparkDto>> connect(@PathVariable String chargingPoint) {
        carparkService.validateIfIsAvailable(chargingPoint);
        carparkService.connect(chargingPoint);
        return ResponseEntity.ok(carparkService.findAllByOrderByIdAsc());
    }

    @PutMapping(CARPARK_URI_WITH_ID_DISCONNECT)
    @ApiOperation(value = "Update charging point disconnecting a car", nickname = "disconnectChargingPoint")
    public ResponseEntity<List<CarparkDto>> disconnect(@PathVariable String chargingPoint) {
        carparkService.disconnect(chargingPoint);
        return ResponseEntity.ok(carparkService.findAllByOrderByIdAsc());
    }
}
