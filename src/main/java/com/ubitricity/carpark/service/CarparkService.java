package com.ubitricity.carpark.service;

import com.ubitricity.carpark.dto.CarparkDto;
import com.ubitricity.carpark.exceptionhandler.ChargingPointAlreadyConnected;
import com.ubitricity.carpark.persistence.model.Carpark;
import com.ubitricity.carpark.persistence.repository.CarparkRepository;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.ubitricity.carpark.util.Constants.*;
import static java.util.function.Predicate.not;

@Service
public class CarparkService extends RuntimeException {

    public CarparkService(CarparkRepository carparkRepository, ModelMapper modelMapper) {
        this.carparkRepository = carparkRepository;
        this.modelMapper = modelMapper;
    }

    private final CarparkRepository carparkRepository;
    private final ModelMapper modelMapper;

    public List<CarparkDto> findAllByOrderByIdAsc() {
        List<Carpark> carpark = carparkRepository.findAllByOrderByIdAsc();
        return carpark.stream().map(this::modelToDto).collect(Collectors.toList());
    }

    public CarparkDto findByChargingPoint(String chargingPoint) {
        Carpark carpark = carparkRepository.findByChargingPoint(chargingPoint);

        return modelToDto(carpark);
    }

    public void connect(String chargingPoint) {
        redistributeEnergyPlugging(carparkRepository.findAllByOrderByIdAsc(), chargingPoint);
    }

    public void disconnect(String chargingPoint) {
        redistributeEnergyUnplugging(carparkRepository.findAllByOrderByIdAsc(), chargingPoint);
    }

    public void redistributeEnergyPlugging(List<Carpark> carparkList, String chargingPoint) {
        Integer availableEnergy = getAvailableEnergy(carparkList);

        if (availableEnergy >= FAST_CHARGE) {
            saveCarparkPlugging(chargingPoint, FAST_CHARGE, true);
        } else {
            List<Carpark> sortedCarparkFastChargeList = filterCarparkWithFastchargeOrderedByStartdate(carparkList);

            if (sortedCarparkFastChargeList.size() == 1) {
                changingToNormalChargeDueLittleAvailableEnergy(sortedCarparkFastChargeList.get(0));
                saveCarparkPlugging(chargingPoint, NORMAL_CHARGE, false);
            } else {
                ListIterator<Carpark> listIterator = sortedCarparkFastChargeList.listIterator();

                while (availableEnergy < FAST_CHARGE) {
                    Carpark currentCarpark = listIterator.next();
                    if (currentCarpark.isFastCharging()) {
                        availableEnergy += NORMAL_CHARGE;
                        changingToNormalChargeDueLittleAvailableEnergy(currentCarpark);
                    }
                }
                saveCarparkPlugging(chargingPoint, FAST_CHARGE, true);
            }
        }
    }

    private void redistributeEnergyUnplugging(List<Carpark> carparkList, String chargingPoint) {
        Integer availableEnergy = getAvailableEnergyButChargingPoint(carparkList, chargingPoint);

        List<Carpark> sortedCarparkNormalChargeList = filterCarparkNormalReverseOrderByStartdateWithoutChargingPoint(
                carparkList, chargingPoint);

        if (!sortedCarparkNormalChargeList.isEmpty()) {
            ListIterator<Carpark> listIterator = sortedCarparkNormalChargeList.listIterator();

            while (availableEnergy > 0) {
                Carpark currentCarpark = listIterator.next();
                changingToFastchargeDueMoreAvailableEnergy(currentCarpark);
                availableEnergy -= NORMAL_CHARGE;
            }
        }

        saveCarparkUnplugging(chargingPoint);
    }

    public boolean validateIfIsAvailable(String chargingPoint) {
        if (carparkRepository.existsByChargingPointAndAvailable(chargingPoint)) {
            throw new ChargingPointAlreadyConnected(chargingPoint);
        }
        return true;
    }

    private void saveCarparkPlugging(String chargingPoint, Integer currentEnergy, Boolean fastCharge) {
        Carpark carpark = carparkRepository.findByChargingPoint(chargingPoint);
        carpark.setCurrentEnergy(currentEnergy);
        carpark.setAvailable(false);
        carpark.setFastCharging(fastCharge);
        carpark.setStartDatetime(LocalDateTime.now());
        carparkRepository.save(carpark);
    }

    private void saveCarparkUnplugging(String chargingPoint) {
        Carpark carpark = carparkRepository.findByChargingPoint(chargingPoint);
        carpark.setCurrentEnergy(0);
        carpark.setAvailable(true);
        carpark.setFastCharging(false);
        carpark.setStartDatetime(null);
        carparkRepository.save(carpark);
    }

    private void changingToNormalChargeDueLittleAvailableEnergy(Carpark carpark) {
        carpark.setCurrentEnergy(NORMAL_CHARGE);
        carpark.setFastCharging(false);
        carparkRepository.save(carpark);
    }

    private void changingToFastchargeDueMoreAvailableEnergy(Carpark carpark) {
        carpark.setCurrentEnergy(FAST_CHARGE);
        carpark.setFastCharging(true);
        carparkRepository.save(carpark);
    }

    @NotNull
    private Integer getAvailableEnergyButChargingPoint(List<Carpark> carparkList, String chargingPoint) {
        return TOTAL_AVAILABLE_ENERGY
                - carparkList.stream().filter(Objects::nonNull).filter(c -> !c.getChargingPoint().equals(chargingPoint))
                        .mapToInt(Carpark::getCurrentEnergy).filter(Objects::nonNull).sum();
    }

    @NotNull
    private Integer getAvailableEnergy(List<Carpark> carparkList) {
        return TOTAL_AVAILABLE_ENERGY - carparkList.stream().filter(Objects::nonNull)
                .mapToInt(Carpark::getCurrentEnergy).filter(Objects::nonNull).sum();
    }

    private List<Carpark> filterCarparkWithFastchargeOrderedByStartdate(List<Carpark> carparkList) {
        return carparkList.stream().filter(Objects::nonNull)
                .sorted(Comparator.comparing(Carpark::getStartDatetime,
                        Comparator.nullsLast(Comparator.naturalOrder())))
                .filter(Objects::nonNull).filter(Carpark::isFastCharging).collect(Collectors.toList());
    }

    private List<Carpark> filterCarparkNormalReverseOrderByStartdateWithoutChargingPoint(List<Carpark> carparkList,
            String chargingPoint) {
        return carparkList.stream().filter(Objects::nonNull)
                .sorted(Comparator.comparing(Carpark::getStartDatetime,
                        Comparator.nullsLast(Comparator.reverseOrder())))
                .filter(Objects::nonNull).filter(not(Carpark::isFastCharging)).filter(not(Carpark::isAvailable))
                .filter(c -> !c.getChargingPoint().equals(chargingPoint)).collect(Collectors.toList());
    }

    private CarparkDto modelToDto(Carpark carpark) {
        return modelMapper.map(carpark, CarparkDto.class);
    }

    private Carpark dtoToModel(CarparkDto dto) {
        return modelMapper.map(dto, Carpark.class);
    }
}
