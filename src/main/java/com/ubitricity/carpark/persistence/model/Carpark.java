package com.ubitricity.carpark.persistence.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tbl_park")
public class Carpark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "charging_point")
    private String chargingPoint;

    @NotNull
    @Column(name = "available")
    private Boolean available;

    @NotNull
    @Column(name = "fast_charge")
    private Boolean fastCharging;

    @Column(name = "current_energy")
    private Integer currentEnergy;

    @Column(name = "start_datetime")
    private LocalDateTime startDatetime;

    public Boolean isAvailable() {
        return this.available;
    }

    public Boolean isFastCharging() {
        return this.fastCharging;
    }
}
