package com.ubitricity.carpark.persistence.repository;

import com.ubitricity.carpark.persistence.model.Carpark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CarparkRepository extends JpaRepository<Carpark, Long> {

    Carpark findByChargingPoint(String chargingPoint);

    List<Carpark> findAllByOrderByIdAsc();

    @Query("select case when (c.currentEnergy > 0) then true else false end "
         + "  from Carpark c "
         + " where c.chargingPoint = :chargingPoint")
    boolean existsByChargingPointAndAvailable(@Param("chargingPoint") String chargingPoint);
}
