package com.ubitricity.carpark.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ubitricity.carpark.persistence.model.serialization.LocalDateTimeDeserializer;
import com.ubitricity.carpark.persistence.model.serialization.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CarparkDto {

    @NotNull
    private String chargingPoint;

    @NotNull
    private Boolean available;

    @NotNull
    private Boolean fastCharging;

    private Integer currentEnergy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startDatetime;
}
