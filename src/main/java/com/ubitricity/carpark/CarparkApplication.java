package com.ubitricity.carpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication(scanBasePackages = "com.ubitricity.carpark")
@EnableJpaRepositories(basePackages = {"com.ubitricity.carpark"})
public class CarparkApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CarparkApplication.class, args);
    }

}
