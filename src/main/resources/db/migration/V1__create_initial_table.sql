CREATE TABLE tbl_park(
  id              BIGSERIAL PRIMARY KEY,

  charging_point  VARCHAR (8) NOT NULL,
  available       BOOLEAN NOT NULL,
  fast_charge     BOOLEAN DEFAULT false,
  current_energy  SMALLINT,
  start_datetime  TIMESTAMP
);

CREATE INDEX idx_park_charging_point ON tbl_park(charging_point);

INSERT INTO
    tbl_park (id, charging_point, available, fast_charge, current_energy, start_datetime)
VALUES
    (1 , 'CP1' , true, false, 0, null),
    (2 , 'CP2' , true, false, 0, null),
    (3 , 'CP3' , true, false, 0, null),
    (4 , 'CP4' , true, false, 0, null),
    (5 , 'CP5' , true, false, 0, null),
    (6 , 'CP6' , true, false, 0, null),
    (7 , 'CP7' , true, false, 0, null),
    (8 , 'CP8' , true, false, 0, null),
    (9 , 'CP9' , true, false, 0, null),
    (10, 'CP10', true, false, 0, null);
