# README #

### Ubitricity Carpark API ###

This is the Ubitricity Carpark API. 
Its goal is to manage the charging points installed at Carpark Ubi. 
Carpark Ubi has 10 charging points installed. 
When a car is connected it consumes either 20 Amperes (fast charging) or 10 Amperes (slow charging). 
Carpark Ubi installation has an overall current input of 100 Amperes so it can support fast charging for a maximum of 5 cars or slow charging for a maximum of 10 cars at one time. 
A charge point notifies the application when a car is plugged or unplugged.
The application must distribute the available current of 100 Amperes among the charging points so that when possible all cars use fast charging and when the current is not sufficient some cars are switched to slow charging. 
Cars which were connected earlier have lower priority than those which were connected later. 
The application must also provide a report with a current state of each charging point returning a list of charging point, status (free or occupied) and - if occupied – the consumed current.


* Version 0.0.2

### Set Up ###
* This project uses [Java 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html), [Spring boot](https://spring.io/projects/spring-boot) and [PostgreSQL](https://www.postgresql.org/)
* You can run the application using Docker Compose.
docker-compose up


# API Documentation
* Swagger-UI is accessible on `http://localhost:8082/swagger-ui.html` for API documentation

# Running Unit Tests
* Run `mvn clean compile test`

### Running the project locally ###
* Once you are running database image on docker-compose, you must run the database migration as described below in DB Migration.
* After, Run `mvn clean install`, get into the target folder and Run `java -jar carpark-api-0.0.2-SNAPSHOT.jar`
* And then, the application must run properly. Optionaly, it's possible to access it from: http://localhost:8082/swagger-ui.html#/
* For developing, you should install lombok plugin in your IDE

### DB Migration ###

* Run `mvn compile -P local flyway:info` to check the migration status
* Run `mvn compile -P local flyway:migrate` to run all migration scripts

### Sonarqube ###
* Run `docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:latest`
* Run `mvn sonar:sonar -Dsonar.host.url=http://localhost:9000`
* http://localhost:9000
