FROM openjdk:11.0.3-jre-slim-stretch
MAINTAINER ubitricity
COPY ./target/carpark-api-0.0.1-SNAPSHOT.jar  /carpark-api.jar
EXPOSE 8082
CMD ["/usr/bin/java", "-jar", "/carpark-api.jar"]
